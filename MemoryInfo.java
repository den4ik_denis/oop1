package com.OOP1;

public class MemoryInfo {
    private String percentOccMemorySpace;
    private int memoryCost;

    public MemoryInfo(String percentOccMemorySpace, int memoryCost) {
        this.percentOccMemorySpace = percentOccMemorySpace;
        this.memoryCost = memoryCost;
    }

    @Override
    public String toString() {
        return "MemoryInfo{" +
                "percentOccMemorySpace='" + percentOccMemorySpace + '\'' +
                ", memoryCost=" + memoryCost +
                '}';
    }

    public String getPercentOccMemorySpace() {
        return percentOccMemorySpace;
    }

    public void setPercentOccMemorySpace(String percentOccMemorySpace) {
        this.percentOccMemorySpace = percentOccMemorySpace;
    }

    public int getMemoryCost() {
        return memoryCost;
    }

    public void setMemoryCost(int memoryCost) {
        this.memoryCost = memoryCost;
    }


}
