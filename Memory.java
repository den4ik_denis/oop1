package com.OOP1;

public class Memory {

    private String[] memoryCell;

    public Memory(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }

    public Memory() {}


    public String readLast() {
        if (memoryCell == null) {
            String last = memoryCell[memoryCell.length - 1];
            return last;
        } else {
            return "Massive empty";
        }
    }

    public String removeLast() {
        memoryCell[memoryCell.length - 1] = null;
        return null;
    }

    public boolean save(String upSave) {
        if (memoryCell != null) {
            if (memoryCell[memoryCell.length - 1] != null) {
                return false;
            }
            for (String str : memoryCell) {
                if (str == null) {
                    str = upSave;
                    return true;
                }
            }
        }
        return false;
    }

    public MemoryInfo getMemoryInfo() {

        if (memoryCell != null) {
            int numbers;
            int percent;
            for (int i = memoryCell.length; i > 0; i++) {
                if ( memoryCell[i] != null){
                    numbers = memoryCell.length - i;
                    percent = i*100/ memoryCell.length;
                    return new MemoryInfo(percent + "%", numbers );
            }
        }
    }return new MemoryInfo("0%", 0);
}

    public String[] getMemoryCell() {
        return memoryCell;
    }

    public void setMemoryCell(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }
}