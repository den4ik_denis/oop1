package com.OOP1;

import java.util.Locale;

public class ProcessorArm extends Processor {

    private final String architecture = "ARM";

    @Override
    String dataProcess(String data) {
        return data.toUpperCase(Locale.ROOT);
    }

    @Override
    String dataProcess(long data) {
        return null;
    }
}
