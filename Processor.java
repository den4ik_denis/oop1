package com.OOP1;

abstract class Processor {

    private String frequency;
    private String cache;
    private int bitCapacity;

    public String getDetails(){
        return frequency + cache + bitCapacity;
    }
    abstract String dataProcess(String data);
    abstract String dataProcess(long data);
}

