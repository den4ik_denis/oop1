package com.OOP1;

import java.util.Locale;

public class ProcessorX86 extends Processor {

    private final String architecture = "X86";

    @Override
    String dataProcess(String data) {
        return data.toLowerCase(Locale.ROOT);
    }

    @Override
    String dataProcess(long data) {
        return null;
    }
}
