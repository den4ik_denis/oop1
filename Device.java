package com.OOP1;

import java.util.Comparator;
import java.util.Locale;
import java.util.Objects;

public class Device implements Comparator<Device> {

    private Processor processor;
    private Memory memory;

    public void save(String[] data) {
        if (memory != null) {
            for (String str : data) {
                if (!memory.save(str)) {
                    return;
                }
            }
        }

    }

    public String[] readAll() {
        if (memory != null && processor != null) {
            String[] data = new String[memory.getMemoryCell().length];
            for (int i = 0; i < data.length; i++) {
                if (memory.readLast() != null) {
                    data[i] = memory.readLast();
                    memory.removeLast();
                }
            }
            return data;
        }
        return new String[0];
    }

    public void dataProcessing() {
        if (memory != null && memory.getMemoryCell() != null) {
            for (int i = 0; i < memory.getMemoryCell().length; i++) {
                if(memory.getMemoryCell()[i] != null){
                    memory.getMemoryCell()[i] = memory.getMemoryCell()[i].toUpperCase(Locale.ROOT);
                }
            }
        }
    }

    public String getSystemInfo() {
        if (memory != null && processor != null) {
            return processor.getDetails();
        }
        return "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return processor.equals(device.processor) && memory.equals(device.memory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processor, memory);
    }

    @Override
    public String toString() {
        return "Device{" +
                "processor=" + processor +
                ", memory=" + memory +
                '}';
    }

    @Override
    public int compare(Device o1, Device o2) {
        return 0;
    }
}
